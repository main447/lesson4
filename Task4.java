import java.util.Arrays;
import java.util.Random;

public class Task4 {
    public static void main(String[] args) {
        Random random = new Random();
        int[] mas = new int[8];
        int c=0;
        for(int i=0;i<8;i++){
            mas[i]=(random.nextInt(10)+1);
        }
        String masst = Arrays.toString(mas);
        System.out.println(masst);
        for(int i=1;i<8;i++){
            if(mas[i]>mas[i-1]){
                c+=1;
            }
        }
        if(c==7){
            System.out.println("массив является строго возрастающей последовательностью");
        }
        else{
            System.out.println("массив не является строго возрастающей последовательностью");
        }
        for(int i=1;i<8;i+=2){
            mas[i]=0;
        }
        String masst1 = Arrays.toString(mas);
        System.out.println(masst1);
    }
}
